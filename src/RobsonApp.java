import java.util.Scanner;

public class RobsonApp {
    public static void main(String[] args) {
        Empresa empresa = new Empresa();
        Scanner entrada = new Scanner(System.in);
        String nome;
        int opcao, cargo, codigo;
        float salario;
        boolean sair = false;

        empresa.cadastraCargo(2500);
        empresa.cadastraCargo(1500);
        empresa.cadastraCargo(10000);
        empresa.cadastraCargo(1200);
        empresa.cadastraCargo(800);

        empresa.cadastraFuncionario(1,15, "Joao da Silva");
        empresa.cadastraFuncionario(2,1, "Pedro Santos");
        empresa.cadastraFuncionario(3,26, "Maria Oliveira");
        empresa.cadastraFuncionario(5,12, "Rita Alcantara");
        empresa.cadastraFuncionario(2,8, "Ligia Matos");

        do {


            System.out.println("\n1. Cadastrar cargo");
            System.out.println("2. Cadastrar funcionario");
            System.out.println("3. Relatorio de funcionarios");
            System.out.println("4. Valor total de salario por cargo");
            System.out.println("5. Sair");
            System.out.print("Escolha sua opção: ");
            opcao = entrada.nextInt();

            switch (opcao) {
                case 1:
                    entrada.nextLine();
                    System.out.println("Informe o valor do salario: ");
                    salario = entrada.nextFloat();
                    //entrada.nextLine();
                    //System.out.println("Informe o codigo do cargo: ");
                    //cargo = entrada.nextInt();
                    empresa.cadastraCargo(salario);


                    break;

                case 2:
                    entrada.nextLine();
                    System.out.println("Digite o nome do funcionario: ");
                    nome = entrada.nextLine();
                    System.out.println("Digite o cargo de " + nome +": ");
                    cargo = entrada.nextInt();
                    entrada.nextLine();
                    System.out.println("Digite o codigo do funcionario: ");
                    codigo = entrada.nextInt();


                    empresa.cadastraFuncionario(cargo, codigo, nome);

                    break;

                case 3:
                    entrada.nextLine();
                    empresa.exibir();
                    break;

                case 4:
                    entrada.nextLine();
                    System.out.println("Digite o codigo do cargo: ");
                    cargo = entrada.nextInt();
                    empresa.mostraSalario(cargo);

                    break;

                case 5:
                    sair = true;
                    System.out.println("\nAte logo! ");
                    break;


                default:
                    System.out.println("\nOpção inválida!\n");
            }




        }
        while(!sair);
        entrada.close();


    }
}