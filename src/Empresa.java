import java.util.ArrayList;

public class Empresa {
    ArrayList<Funcionario> funcionarios = new ArrayList<Funcionario>();
    ArrayList<Cargo> cargos = new ArrayList<Cargo>();
    float somaSalario = 0;


    public void cadastraFuncionario(int c, int codigo, String nome) {
        try {
                for (int x = 0; x < funcionarios.size(); x++) {
                    if (codigo == funcionarios.get(x).getCodigo()) {
                        throwException();
                    }
                }

                for (int i = 0; i < cargos.size(); i++) {
                    if (c == cargos.get(i).getCargo()) {
                        funcionarios.add(new Funcionario(c, cargos.get(i).getSalario(), codigo, nome));
                        System.out.println("\nFuncionario cadastrado com sucesso!\n");
                    }
                }
        }
            catch(Exception e) {
            System.err.println("\nCodigo de usuario ja existente. \n ");
        }
    }


    public void cadastraCargo(float salario) {

        try {
            cargos.add(new Cargo((cargos.size() + 1), salario));
            System.out.println("\nCargo cadastrado com sucesso!\n");
        }
		catch(Exception e) {
            System.err.println("\nERRO\n ");
        }
    }


    public void exibir() {
        for(int i = 0; i < funcionarios.size(); i++) {
            System.out.println("\nNome: " + funcionarios.get(i).getNome() + "    Cargo: " + funcionarios.get(i).getCargo() + "\nSalario: R$" + funcionarios.get(i).getSalario() + "\nID do funcionario: " + funcionarios.get(i).getCodigo() + "\n");
        }
    }


    public void mostraSalario(int c) {
        try {
            somaSalario = 0;
            for (int i = 0; i < funcionarios.size(); i++) {
                if (c == funcionarios.get(i).getCargo()) {
                    System.out.println("\nNome: " + funcionarios.get(i).getNome() + "  Cargo: " + funcionarios.get(i).getCargo() + "\nSalario: R$" + funcionarios.get(i).getSalario());
                    somaSalario = somaSalario + funcionarios.get(i).getSalario();
                }
            }

            if(somaSalario == 0) {
                throwException();
            }

            System.out.println("\nSoma total dos salarios do cargo " + c + ": R$" + somaSalario);
        }
        catch(Exception e) {
            System.err.println("\nCargo nao existente/utilizado\n ");
        }
    }


    public static void throwException() throws Exception {
        try {
            throw new Exception();
        }
        catch(Exception e) {
            throw e;
        }



    }
}


