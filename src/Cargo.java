public class Cargo {
    private int cargo;
    private float salario;


    public Cargo(int cargo, float salario) {
        this.cargo = cargo;
        this.salario = salario;
    }

    public int getCargo() {
        return cargo;
    }

    public void setCargo(int cargo) {
        this.cargo = cargo;
    }

    public float getSalario() {
        return salario;
    }

    public void setSalario(float salario) {
        this.salario = salario;
    }
}
