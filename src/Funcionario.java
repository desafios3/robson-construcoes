public class Funcionario extends Cargo {

    private int codigo;
    private String nome;


    public Funcionario(int cargo, float salario, int codigo, String nome) {
        super(cargo, salario);
        this.codigo = codigo;
        this.nome = nome;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
